jQuery.getJSON('http://api.fixer.io/latest?base=JPY&symbols=USD', { get_param: 'value' }, function(data) {
    jQuery(".item-price,.cap-price").filter(function() {
        var sd = jQuery(this).text().replace(/,/g , '');
        var num = sd.match(/[\d\.]+/g);

        if (num != null){
            var numb = num.toString();
            var jpy_rates = data.rates.JPY;
            var price = parseInt(numb) * parseFloat(jpy_rates);
            var final_price = price.toLocaleString();
            jQuery(this).text('¥ '+final_price);
        }

    });
});



jQuery('.currency-switcher').change(function() {

    if (jQuery(this).val() == "JPY"){
        jQuery.getJSON('http://api.fixer.io/latest?base=USD&symbols=JPY', { get_param: 'value' }, function(data) {
            jQuery(".item-price,.cap-price").filter(function() {
                var sd = jQuery(this).text().replace(/,/g , '');
                var num = sd.match(/[\d\.]+/g);

                if (num != null){
                    var numb = num.toString();
                    var jpy_rates = data.rates.JPY;
                    var price = parseInt(numb) * parseFloat(jpy_rates);
                    var final_price = price.toLocaleString();
                    jQuery(this).text('¥ '+final_price);
                    console.log(price);
                }

            });
        });
    } else if (jQuery(this).val() == "USD"){
        jQuery.getJSON('http://api.fixer.io/latest?base=JPY&symbols=USD', { get_param: 'value' }, function(data) {
            jQuery(".item-price,.cap-price").filter(function() {
                var sd = jQuery(this).text().replace(/,/g , '');
                var num = sd.match(/[\d\.]+/g);

                if (num != null){
                    var numb = num.toString();
                    var usd_rates = data.rates.USD;
                    var price = parseInt(numb) * parseFloat(usd_rates);
                    var final_price = price.toLocaleString();
                    jQuery(this).text('$ '+final_price+' USD');
                }

            });
        });
    }
});
