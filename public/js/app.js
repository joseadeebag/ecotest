$(function () {
    var App = {

        /**
         * Init Function
         */
        init: function() {
            App.Animations();
            App.Miscellaneous();
            App.ToggleClass();
            App.Carousel();
            App.ScrollBack();
        },

        Animations: function(){
            $('.grid-list li').hover(
                function () {
                    $(this).find('.poster').css('opacity', '0');
                    $(this).find('.overlay').css('opacity', '1');
                },
                function () {
                    $(this).find('.poster').css('opacity', '1');
                    $(this).find('.overlay').css('opacity', '0');
                }
            );
        },


        /**
         * Miscellaneous
         */
        Miscellaneous: function() {

            $.material.init();

            $('.movie-info[data-toggle="popover"]').webuiPopover({
                trigger:'hover',
                style:'inverse',
                animation:'pop',
                width:'350px'
            });

            $('a[data-toggle="popover"]').webuiPopover({
                trigger:'hover',
                style:'inverse',
                animation:'pop'
            });

            $('[data-toggle="tooltip"]').tooltip();

            $('#btn-search').click(function(){
                $('#formsearch').slideToggle( "fast",function(){
                    $( '#content' ).toggleClass( "moremargin" );
                } );
                $('#searchbox').focus();
                $('.openclosesearch').toggle();
            });

            $('.selectpicker').selectpicker();

            $("[data-type=forgotpass]").click(function(){
                $('#loginModal').modal('hide');
            });
        },


        /**
         * Toggle Class
         */
        ToggleClass: function() {
            $(document).on('click', '.track-play [data-toggle^="class"]', function(e){
                e && e.preventDefault();
                var $this = $(e.target), $class , $target, $tmp, $classes, $targets;
                !$this.data('toggle') && ($this = $this.closest('[data-toggle^="class"]'));
                $class = $this.data()['toggle'];
                $target = $this.data('target') || $this.attr('href');
                $class && ($tmp = $class.split(':')[1]) && ($classes = $tmp.split(','));
                $target && ($targets = $target.split(','));
                $classes && $classes.length && $.each($targets, function( index, value ) {
                    if ( $classes[index].indexOf( '*' ) !== -1 ) {
                        var patt = new RegExp( '\\s' +
                            $classes[index].
                            replace( /\*/g, '[A-Za-z0-9-_]+' ).
                            split( ' ' ).
                            join( '\\s|\\s' ) +
                            '\\s', 'g' );
                        $($this).each( function ( i, it ) {
                            var cn = ' ' + it.className + ' ';
                            while ( patt.test( cn ) ) {
                                cn = cn.replace( patt, ' ' );
                            }
                            it.className = $.trim( cn );
                        });
                    }
                    ($targets[index] !='#') && $($targets[index]).toggleClass($classes[index]) || $this.toggleClass($classes[index]);
                });
                $this.toggleClass('active');
            });
        },


        /**
         * Carousel
         */
        Carousel: function() {
            var hmbs = $('#home-moviebox');
            // Homepage: Moviebox Slider
            hmbs.owlCarousel({
                items: 6,
                loop: false,
                autoplay: true,
                rewind: true,
                autoplayHoverPause: true,
                autoplayTimeout:4000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:3
                    },
                    600:{
                        items: 4
                    },
                    1000:{
                        items:6
                    }
                }
            });

            //Navigation For Homepage Moviebox Slider
            hmbs.owlCarousel();
            // Go to the next item
            $('#hmbl').click(function() {
                hmbs.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#hmbr').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                hmbs.trigger('next.owl.carousel', [300]);
            });


            var htbs = $('#home-trailerbox');
            // Homepage: Trailerbox Slider
            htbs.owlCarousel({
                items: 6,
                loop: false,
                autoplay: true,
                rewind: true,
                autoplayHoverPause: true,
                autoplayTimeout:6000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:3
                    },
                    600:{
                        items: 4
                    },
                    1000:{
                        items:6
                    }
                }
            });
            //Navigation For Homepage Trailerbox Slider
            htbs.owlCarousel();
            // Go to the next item
            $('#htbl').click(function() {
                htbs.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#htbr').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                htbs.trigger('next.owl.carousel', [300]);
            });

            var hmubs = $('#home-musicbox');
            hmubs.owlCarousel({
                items: 3,
                lazyLoad:true,
                rewind: true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1
                    },
                    475:{
                        items:2
                    },
                    600:{
                        items: 3
                    }
                }
            });
            //Navigation For Homepage Trailerbox Slider
            hmubs.owlCarousel();
            // Go to the next item
            $('#hmubl').click(function() {
                hmubs.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#hmubr').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                hmubs.trigger('next.owl.carousel', [300]);
            });

            var hebs = $('#home-eyebox');
            hebs.owlCarousel({
                items: 2,
                lazyLoad:true,
                rewind: true,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1
                    },
                    475:{
                        items:1
                    },
                    600:{
                        items: 2
                    },
                    1000:{
                        items: 2
                    }
                }
            });

            var tgs = $('#genre-slider');
            tgs.owlCarousel({
                items: 6,
                autoplay: true,
                rewind: true,
                loop: false,
                lazyLoad:true,
                autoplayHoverPause: true,
                autoplayTimeout:5000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:3
                    },
                    600:{
                        items: 5
                    },
                    1000:{
                        items: 6
                    }
                }
            });
            //Navigation For Tunebox Genre Slider
            tgs.owlCarousel();
            // Go to the next item
            $('#tgl').click(function() {
                tgs.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#tgr').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                tgs.trigger('next.owl.carousel', [300]);
            });


            var tas = $('#artist-slider');
            tas.owlCarousel({
                items: 2,
                autoplay: true,
                rewind: true,
                loop: false,
                lazyLoad:true,
                autoplayHoverPause: true,
                autoplayTimeout:7000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:2
                    },
                    600:{
                        items: 2
                    },
                    1000:{
                        items: 2
                    }
                }
            });
            tas.owlCarousel();
            // Go to the next item
            $('#tal').click(function() {
                tas.trigger('prev.owl.carousel', [1000]);
            });
            // Go to the previous item
            $('#tar').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                tas.trigger('next.owl.carousel', [1000]);
            });


            var tts = $('#tunebox-trending');
            tts.owlCarousel({
                items: 5,
                autoplay: true,
                rewind: true,
                loop: false,
                lazyLoad:true,
                autoplayHoverPause: true,
                autoplayTimeout:4000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:2
                    },
                    600:{
                        items: 2
                    },
                    1000:{
                        items: 5
                    }
                }
            });
            //Navigation For Tunebox Trending Songs Slider
            tts.owlCarousel();
            // Go to the next item
            $('#ttl').click(function() {
                tts.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#ttr').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                tts.trigger('next.owl.carousel', [300]);
            });

            var sts = $('#songs-trending');
            sts.owlCarousel({
                items: 4,
                autoplay: true,
                rewind: true,
                loop: false,
                lazyLoad:true,
                autoplayHoverPause: true,
                autoplayTimeout:6000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:3
                    },
                    600:{
                        items: 3
                    },
                    1000:{
                        items: 4
                    }
                }
            });
            //Navigation For Tunebox Trending Songs Slider
            sts.owlCarousel();
            // Go to the next item
            $('#stl').click(function() {
                sts.trigger('prev.owl.carousel', [300]);
            });
            // Go to the previous item
            $('#str').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                sts.trigger('next.owl.carousel', [300]);
            });

            var mvid = $('#musicbox-videos');
            mvid.owlCarousel({
                items: 2,
                autoplay: true,
                rewind: true,
                loop: false,
                lazyLoad:true,
                autoplayHoverPause: true,
                autoplayTimeout:6000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1
                    },
                    475:{
                        items:1
                    },
                    600:{
                        items: 1
                    },
                    1000:{
                        items: 2
                    }
                }
            });

            var datetime = $('#date-time');
            //Date Time: Date Time Slider
            datetime.owlCarousel({
                items: 4,
                loop: false,
                autoplay: true,
                rewind: true,
                autoplayHoverPause: true,
                autoplayTimeout:4000,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:2
                    },
                    475:{
                        items:2
                    },
                    600:{
                        items: 2
                    },
                    1000:{
                        items:4
                    }
                }
            });

        },


        /**
         * ScrollBack
         */
        ScrollBack: function() {
            var btop = '#back-to-top';
            if ($(btop).length) {
                var scrollTrigger = 100, // px
                    backToTop = function () {
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop > scrollTrigger) {
                            $(btop).addClass('show');
                        } else {
                            $(btop).removeClass('show');
                        }
                    };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $(btop).on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }
        }
    };
    $(function() {
        App.init();
    });

});