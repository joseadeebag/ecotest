<?php namespace App\Http\Controllers;


use Aws\S3\S3Client;
use DB;
use Illuminate\Support\Facades\Log;
use Auth;
use Hash;
use File;
class HomeController extends Controller {


	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	//	$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
/*		 $hotbox = new \SplDoublyLinkedList();// musicvideoS  hotbox
		 $hot = \DB::connection('musictable')->select('SELECT musicbox_id FROM `musicboxes_playlists` WHERE `playlist_id`=1');

		   foreach ($hot as $cat){//music_id music_id
            $data = \DB::connection('musictable')->select('SELECT * FROM `musicboxes` WHERE `music_id`='.$cat->musicbox_id);
            $data2 = \DB::connection('video')->select('SELECT filename FROM `videos` WHERE `id`='.$data[0]->video_id);
            //$resourceKey=$this->getVideoUrl($data2[0]->filename);
            $client = S3Client::factory(array(
                 'key' => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
                'region' => env('AWS_REGION'),
                 'version' => 'latest'
            ));
            $bucket='domusicbox';
            $signedUrl = $client->getObjectUrl($bucket, $data2[0]->filename, '+500 minutes');

            $row =array("music_id"=>$data[0]->music_id,"music_artiste"=> $data[0]->music_artiste,"music_title"=>$data[0]->music_title,"music_image"=>$data[0]->music_filename, "music_views"=>$data[0]->music_views, "video_filename"=>$signedUrl);
            $hotbox->push($row);
        }*/




        //return view('index')->with(array(/*'recom'=>$video_recommended, 'inbox'=>$inbox, 'vjeos'=>$vjeos,*/ 'hotbox'=>$hotbox ));



		return view('admin_dashboard');
	}

}
