@extends('layouts.ba')
@section('title')
   Admin Backend
@endsection

@section('styles')

<link rel="stylesheet" type="text/css" href="../backend/vendors/css/tables/jsgrid/jsgrid-theme.min.css">
<link rel="stylesheet" type="text/css" href="../backend/vendors/css/tables/jsgrid/jsgrid.min.css">

@endsection

@section('content')

  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Manage User</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/admin')}}">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="{{url('/admin/user')}}">User Table</a>
                </li>
                
              </ol>
            </div>
          </div>
        </div>

        <div class="content-header-right col-md-6 col-12">
          <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
            <div role="group" class="btn-group">
              <button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings icon-left"></i> Settings</button>
              <div aria-labelledby="btnGroupDrop1" class="dropdown-menu"><a href="{{url('/admin/search_user')}}" class="dropdown-item">Search User</a>
                
              </div>
            </div>
            
          </div>
        </div>

      </div>

      <div class="content-body">
        <!-- Basic Tables start -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Users info</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                 


                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Username</th>
                          <th>Gender</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Date Regisetered</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach ($user as $m)
                        <tr class="<?php if($m['status']=='1'){

                          }else{
                              echo "bg-warning";
                            }?>">
                          <th scope="row">{{$m['id']}}</th>
                          <td>{{$m['username']}}</td>
                          <td>{{$m['gender']}}</td>
                          <td>{{$m['firstname'].' '.$m['lastname']}}</td>
                           <td>{{$m['email']}}</td>
                            <td>{{$m['phone']}}</td>
                             <td>{{$m['date_entered']}}</td>
                              <td>
                              	<div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                            <div class="dropdown-menu" x-placement="bottom-start" >
                              <a class="dropdown-item" href="{{url('/view_user')}}/{{$m['id']}}">View</a>
                              <a class="dropdown-item" href="{{url('/edit_user')}}/{{$m['id']}}">Edit</a>
                              <?php 
                                if ($m['status']=='1'){
                                  ?>
                              
                                     <a class="dropdown-item" id="disableacc" href="{{url('/disable_user')}}/{{$m['id']}}">Disable Account</a>  

                              <?php
                                }else{

                                  ?>

                                   <a class="dropdown-item" id="enableacc" href="{{url('/enable_user')}}/{{$m['id']}}">Enable Account</a> 
                                  <?php
                                }
                              ?>
                                                            
                            </div>
                          </div>
                              </td>
                        </tr>
                        @endforeach
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                 {!! str_replace('/?', '?', $user->render()) !!}
                {{--  {!! $user->render() !!}  --}}
              </div>
            </div>
          </div>
        </div>
        <!-- Basic Tables end -->


     

     


    
      </div>
    </div>
  </div>

@endsection

 @section('scripts')
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../backend/vendors/js/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
  <script src="../backend/vendors/js/tables/jsgrid/griddata.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->

    <!-- BEGIN PAGE LEVEL JS-->
  <script src="../backend/js/scripts/tables/jsgrid/jsgrid.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
  <script type="text/javascript">

     $("#disableacc").on('click', function(e) {
        e.preventDefault();
        var a_href = $(this).attr('href');
        // http://localhost:8000/disable_user/#
        const input =a_href ;
        const [first, second] = input.split('disable_user/')
        crossite();
         $.ajax({
          type: 'POST',
          url: '/disable_user',  
          data:{key:second},
          success: function (data) { 
                console.log(data);
                if(data.msg==1){

                 /* $('#msgerror2').hide();
                  $('#msgerror1').hide();
                  $('#msgerror0').html('Logging you in...');
                  $('#msgerror0').show()
                  $('#registerModal').modal().hide();*/
                  // $('#joseadebag').load(document.URL +  '#joseadebag'); 
                  location.reload();
                }else{
                  /*$('#msgerror1').hide();
                  $('#msgerror0').hide();
                  $('#msgerror2').html(data);
                  $('#msgerror2').show()*/;
                  //return; 
                }      
                                   
         }
       });




     });

     $("#enableacc").on('click', function(e) {
           e.preventDefault();//enable_user
           var a_href = $(this).attr('href');
           const input =a_href ;// spliting string in a smart way
           const [first, second] = input.split('enable_user/')
           //alert(second);
           crossite();
            $.ajax({
          type: 'POST',
          url: '/enable_user',
          data:{key:second},
          success: function (data) { 
                console.log(data);
                if(data.msg==1){

                  /*$('#msgerror2').hide();
                  $('#msgerror1').hide();
                  $('#msgerror0').html('Logging you in...');
                  $('#msgerror0').show()
                  $('#registerModal').modal().hide();*/
                  // $('#joseadebag').load(document.URL +  '#joseadebag'); 
                  location.reload();
                }else{
                 /* $('#msgerror1').hide();
                  $('#msgerror0').hide();
                  $('#msgerror2').html(data);
                  $('#msgerror2').show();*/
                  //return; 
                }      
                                   
         }
       });

     });

     function crossite(){
        $.ajaxSetup({
       // force ajax call on all browsers
                    cache: false,

                    // Enables cross domain requests
                    crossDomain: true,

                    // Helps in setting cookie
                    xhrFields: {
                        withCredentials: true
                    },

                    beforeSend: function (xhr, type) {
                        // Set the CSRF Token in the header for security
                        /*if (type.type !== "GET") {
                            var token = $('meta[name="csrf-token"]').attr('content');//Cookies.get("CSRF-TOKEN");
                            xhr.setRequestHeader('X-XSRF-Token', token);
                        }*/
                    }
     });

     }




  </script>


  @endsection