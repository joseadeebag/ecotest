@extends('layouts.ba')
@section('title')
   Admin Backend
@endsection

@section('styles')


@endsection

@section('content')

  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Edit User</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a>
                </li>
                <li class="breadcrumb-item active"><a href="#">Edit User</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
        {{--   <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
            <div role="group" class="btn-group">
              <button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings icon-left"></i> Settings</button>
              <div aria-labelledby="btnGroupDrop1" class="dropdown-menu"><a href="card-bootstrap.html" class="dropdown-item">Bootstrap Cards</a>
                <a href="component-buttons-extended.html" class="dropdown-item">Buttons Extended</a>
              </div>
            </div>
            <a href="full-calender-basic.html" class="btn btn-outline-primary"><i class="ft-mail"></i></a>
            <a href="timeline-center.html" class="btn btn-outline-primary"><i class="ft-pie-chart"></i></a>
          </div> --}}
        </div>
      </div>
      <div class="content-body">
      
        <!-- Form control repeater section start -->
        <section id="form-control-repeater">
          <div class="row">
          <div class="col-md-2"></div>

            <div class="col-md-8">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="file-repeater">Edit User Info</h4>
                  <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form class="form row">
                      <div class="form-group col-md-6 mb-2">
                        <input type="text" class="form-control" placeholder="<?php if(isset($user[0]->firstname)){
                            echo $user[0]->firstname;
                          }else{
                             echo 'Firstname';

                            }?>" id="firstname" value="<?php if(isset($user[0]->firstname)){
                            echo $user[0]->firstname;
                          }else{
                            

                            }?>" name="firstname">
                      </div>
                      <div class="form-group col-md-6 mb-2">
                        <input type="text" class="form-control" placeholder="<?php if(isset($user[0]->lastname)){
                            echo $user[0]->lastname;
                          }else{
                             echo 'Lastname';

                            }?>" id="lastname" value="<?php if(isset($user[0]->lastname)){
                            echo $user[0]->lastname;
                          }else{
                            

                            }?>" name="lastname">
                      </div>
                      <div class="form-group col-md-6 mb-2">
                        <input type="text" class="form-control" disable placeholder="<?php if(isset($user[0]->email)){
                            echo $user[0]->email;
                          }else{
                             echo 'Email';

                            }?>" value="<?php if(isset($user[0]->email)){
                            echo $user[0]->email;
                          }else{
                            

                            }?>" id="email" name="email">
                      </div>
                      <div class="form-group col-md-6 mb-2">
                        <input type="text" class="form-control" placeholder="<?php if(isset($user[0]->phone)){
                            echo $user[0]->phone;
                          }else{
                             echo 'Phone';

                            }?>" id="phone" value="<?php if(isset($user[0]->phone)){
                            echo $user[0]->phone;
                          }else{
                            

                            }?>" name="phone">
                      </div>
                      <button type="button" style="text-align: center;" id="edituser"  class="btn btn-primary">
                          <i class="icon-plus4"></i> Edit User Profile
                     </button>
                    
                 {{--      <div class="form-group col-12 mb-2 file-repeater">
                        <div data-repeater-list="repeater-list">
                          <div data-repeater-item>
                            <div class="row mb-1">
                              <div class="col-9 col-xl-10">
                                <label class="file center-block">
                                  <input type="file" id="file">
                                  <span class="file-custom"></span>
                                </label>
                              </div>
                              <div class="col-2 col-xl-1">
                                <button type="button" data-repeater-delete class="btn btn-icon btn-danger mr-1"><i class="ft-x"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <button type="button" data-repeater-create class="btn btn-primary">
                          <i class="icon-plus4"></i> Add new file
                        </button>
                      </div> --}}
                     
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-2"></div>
          </div>
        </section>
        <!-- // Form control repeater section end -->
      </div>
    </div>
  </div>


@endsection

@section('scripts')
//edituser
<script type="text/javascript">
        $("#edituser").on('click', function(e) {
          var firstname = $('#firstname').val();
          var lastname = $('#lastname').val();
          var email = $('#email').val();
           var phone = $('#phone').val();
          //alert(passwordold);
          if($('#firstname').val() == ''){  
              // $('#msgerror11').html('Please supply your current Password.');
               //$('#msgerror11').show();
               alert('Please supply your current Firstname.');
                 return;
          }
          if($('#lastname').val() == ''){  
              // $('#msgerror11').html('Please supply your current Password.');
               //$('#msgerror11').show();
                alert('Please supply your current Lastname.');
                 return;
          }
          if($('#email').val() == ''){  
              // $('#msgerror11').html('Please supply your current Password.');
               //$('#msgerror11').show();
                alert('Please supply your current email.');
                 return;
          }
          if($('#phone').val() == ''){  
              // $('#msgerror11').html('Please supply your current Password.');
               //$('#msgerror11').show();
                alert('Please supply your current phone.');
                 return;
          }
           $.ajaxSetup({
       // force ajax call on all browsers
                    cache: false,

                    // Enables cross domain requests
                    crossDomain: true,

                    // Helps in setting cookie
                    xhrFields: {
                        withCredentials: true
                    },

                    beforeSend: function (xhr, type) {
                        // Set the CSRF Token in the header for security
                        /*if (type.type !== "GET") {
                            var token = $('meta[name="csrf-token"]').attr('content');//Cookies.get("CSRF-TOKEN");
                            xhr.setRequestHeader('X-XSRF-Token', token);
                        }*/
                    }
     });

            $.ajax({
          type: 'POST',
          url: '/edituserdetails',
          data:{key:{{$user[0]->id}}, firstname: firstname, email: email, phone:phone, lastname:lastname },
          success: function (data) { 
                console.log(data);
                if(data.msg == 1){
                  alert('User Details Updated');
                 /* $('#msgerror15').hide();
                  $('#msgerror14').hide();
                  $("#msgerror13").html('A recovery email has been sent to your email, kindly follow the link sent to you.');
                  $('#msgerror13').css("visibility", "visible");
                  $('#msgerror13').show();*/
                 // location.reload();

                }else{
                  alert('An error occurred while updating user details.');
                  /* $('#msgerror15').hide();
                   $('#msgerror14').hide();
                   $("#msgerror13").html(data.msg);
                   $('#msgerror13').css("visibility", "visible");;
                   $('#msgerror13').show();
                */}                      
         }
       });

        //alert('am here');
        
       // window.location.href = '/login_history/'+{{$user[0]->id}};

       });
    

      </script>
 
@endsection