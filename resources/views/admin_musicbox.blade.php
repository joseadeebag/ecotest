

@extends('layouts.ba')
@section('title')
   Admin Backend
@endsection

@section('styles')
   
@endsection

@section('content')

   <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Search Users</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Search</a>
                </li>
                <li class="breadcrumb-item active">Search 
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
        {{--   <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
            <div role="group" class="btn-group">
              <button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings icon-left"></i> Settings</button>
              <div aria-labelledby="btnGroupDrop1" class="dropdown-menu"><a href="card-bootstrap.html" class="dropdown-item">Bootstrap Cards</a>
                <a href="component-buttons-extended.html" class="dropdown-item">Buttons Extended</a>
              </div>
            </div>
            <a href="full-calender-basic.html" class="btn btn-outline-primary"><i class="ft-mail"></i></a>
            <a href="timeline-center.html" class="btn btn-outline-primary"><i class="ft-pie-chart"></i></a>
          </div> --}}
        </div>
      </div>
      <div class="content-body">
        <!-- Search form-->
        <div id="search-videos" class="card overflow-hidden">
          <div class="card-header">
            <h4 class="card-title">Search</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
              </ul>
            </div>
          </div>
          <div class="card-content">
            <!--Search Form-->
            <div class="card-body pb-0">
              <fieldset class="form-group position-relative mb-0">
                <input type="text" class="form-control form-control-xl input-xl" value="<?php if(isset($word)){
                  echo $word;
                  }?>" id="iconLeft1" placeholder="Search for Users (with name, email address, Username and phone number) ...">
                <div class="form-control-position">
                  <i class="ft-search font-medium-4" id="searchicon"></i>
                </div>
              </fieldset>
            </div>
            <!--/Search Form-->
            <!--Search Navbar-->
         {{--    <div id="search-nav" class="px-2 py-1">
              <ul class="nav nav nav-inline">
                <li class="nav-item">
                  <a class="nav-link" href="search-website.html"><i class="fa fa-link"></i> Website</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="search-images.html"><i class="fa fa-file-image-o"></i> Images</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="search-videos.html"><i class="fa fa-file-video-o"></i> Videos</a>
                </li>
                
                </li>
              </ul>
            </div> --}}
            <!--/ Search Navbar-->
            <div class="table-responsive" style="padding-top: 20px;">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Username</th>
                          <th>Gender</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Date Regisetered</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if(isset($user))
                        @forelse ($user as $m)
                            <tr class="<?php if($m['status']=='1'){

                          }else{
                              echo "bg-warning";
                            }?>">
                          <th scope="row">{{$m['id']}}</th>
                          <td>{{$m['username']}}</td>
                          <td>{{$m['gender']}}</td>
                          <td>{{$m['firstname'].' '.$m['lastname']}}</td>
                           <td>{{$m['email']}}</td>
                            <td>{{$m['phone']}}</td>
                             <td>{{$m['date_entered']}}</td>
                              <td>
                                <div class="btn-group">
                            <button type="button" class="btn btn-info dropdown-toggle mr-1 mb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                            <div class="dropdown-menu" x-placement="bottom-start" >
                              <a class="dropdown-item" href="{{url('/view_user')}}/{{$m['id']}}">View</a>
                              <a class="dropdown-item" href="{{url('/edit_user')}}/{{$m['id']}}">Edit</a>
                              <?php 
                                if ($m['status']=='1'){
                                  ?>
                              
                                     <a class="dropdown-item" id="disableacc" href="{{url('/disable_user')}}/{{$m['id']}}">Disable Account</a>  

                              <?php
                                }else{

                                  ?>

                                   <a class="dropdown-item" id="enableacc" href="{{url('/enable_user')}}/{{$m['id']}}">Enable Account</a> 
                                  <?php
                                }
                              ?>
                                                            
                            </div>
                          </div>
                              </td>
                        </tr>
                        @empty
                            <p>No users</p>
                        @endforelse
                        @endif
                        
                      </tbody>
                    </table>
                  </div>

                    @if(isset($word))

                       {!! str_replace('/?', '?', $user->render()) !!}
                      {{--   {!! $user->render() !!}  --}}
                    @endif
           </div>
        </div>
      </div>
    </div>
  </div>

@endsection

 @section('scripts')
  <script type="text/javascript">
  //   iconLeft1 searchicon
      $('#iconLeft1').on("keyup", function(e) {
        if (e.keyCode == 13) {
            //alert('Enter');
            search();
        }
      });


       $("#searchicon").on('click', function(e) {

        //alert('Enter');
        search();

       });

       function search(){
          //#iconLeft1
         var key= $('#iconLeft1').val();
         if($('#iconLeft1').val().length <=0){
           alert('Please enter a search word');
           return;

         }
          window.location.href = '/search_user/'+key;

          /*$.ajax({
          type: 'POST',
          url: '/searchuser',
          data:{key:key},
          success: function (data) { 
                console.log(data);
                if(data==1){
                  // $('#joseadebag').load(document.URL +  '#joseadebag'); 
                  location.reload();
                }else{
                
              
                }      
                                   
         }
       });*/



       }

  </script>
  <script type="text/javascript">

     $("#disableacc").on('click', function(e) {
        e.preventDefault();
        var a_href = $(this).attr('href');
        // http://localhost:8000/disable_user/#
        const input =a_href ;
        const [first, second] = input.split('disable_user/')
        crossite();
         $.ajax({
          type: 'POST',
          url: '/disable_user',  
          data:{key:second},
          success: function (data) { 
                console.log(data);
                if(data.msg==1){

                 /* $('#msgerror2').hide();
                  $('#msgerror1').hide();
                  $('#msgerror0').html('Logging you in...');
                  $('#msgerror0').show()
                  $('#registerModal').modal().hide();*/
                  // $('#joseadebag').load(document.URL +  '#joseadebag'); 
                  location.reload();
                }else{
                  /*$('#msgerror1').hide();
                  $('#msgerror0').hide();
                  $('#msgerror2').html(data);
                  $('#msgerror2').show()*/;
                  //return; 
                }      
                                   
         }
       });




     });

     $("#enableacc").on('click', function(e) {
           e.preventDefault();//enable_user
           var a_href = $(this).attr('href');
           const input =a_href ;// spliting string in a smart way
           const [first, second] = input.split('enable_user/')
           //alert(second);
           crossite();
            $.ajax({
          type: 'POST',
          url: '/enable_user',
          data:{key:second},
          success: function (data) { 
                console.log(data);
                if(data.msg==1){

                  /*$('#msgerror2').hide();
                  $('#msgerror1').hide();
                  $('#msgerror0').html('Logging you in...');
                  $('#msgerror0').show()
                  $('#registerModal').modal().hide();*/
                  // $('#joseadebag').load(document.URL +  '#joseadebag'); 
                  location.reload();
                }else{
                 /* $('#msgerror1').hide();
                  $('#msgerror0').hide();
                  $('#msgerror2').html(data);
                  $('#msgerror2').show();*/
                  //return; 
                }      
                                   
         }
       });

     });

     function crossite(){
        $.ajaxSetup({
       // force ajax call on all browsers
                    cache: false,

                    // Enables cross domain requests
                    crossDomain: true,

                    // Helps in setting cookie
                    xhrFields: {
                        withCredentials: true
                    },

                    beforeSend: function (xhr, type) {
                        // Set the CSRF Token in the header for security
                        /*if (type.type !== "GET") {
                            var token = $('meta[name="csrf-token"]').attr('content');//Cookies.get("CSRF-TOKEN");
                            xhr.setRequestHeader('X-XSRF-Token', token);
                        }*/
                    }
     });

     }




  </script>
  
  @endsection