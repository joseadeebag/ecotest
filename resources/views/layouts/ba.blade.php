<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Dec 2017 10:13:28 GMT -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="PIXINVENT">
  <title>@yield('title')</title>
  <link rel="apple-touch-icon" href="../backend/images/ico/apple-icon-120.png">
  <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
  rel="stylesheet">
  <!-- BEGIN VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="../backend/css/vendors.css">
  <link rel="stylesheet" type="text/css" href="../backend/vendors/css/extensions/unslider.css">
  <link rel="stylesheet" type="text/css" href="../backend/vendors/css/weather-icons/climacons.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/fonts/meteocons/style.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/vendors/css/charts/morris.css">
  <!-- END VENDOR CSS-->
  <!-- BEGIN STACK CSS-->
  <link rel="stylesheet" type="text/css" href="../backend/css/app.min.css">
  <!-- END STACK CSS-->
  <!-- BEGIN Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="../backend/css/core/menu/menu-types/vertical-menu.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/css/core/colors/palette-gradient.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/fonts/simple-line-icons/style.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/css/core/colors/palette-gradient.min.css">
  <link rel="stylesheet" type="text/css" href="../backend/css/pages/timeline.min.css">
  <!-- END Page Level CSS-->
  <!-- BEGIN Custom CSS-->
  <link rel="stylesheet" type="text/css" href="../backend/css/style.css">
   <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('styles')
  <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
  <!-- - var navbarShadow = true-->
  <!-- fixed-top-->
@include('admin_includes.admin_nav')
  <!-- ////////////////////////////////////////////////////////////////////////////-->
@include('admin_includes.admin_sidebar')
@yield('content')
  
  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <!--  --> 
  <!-- @include('admin_includes.admin_customizer') -->

  @include('admin_includes.admin_footer')
  <!-- BEGIN VENDOR JS-->
  <script src="../backend/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../backend/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="../backend/vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="../backend/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="../backend/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="../backend/js/core/app-menu.min.js" type="text/javascript"></script>
  <script src="../backend/js/core/app.min.js" type="text/javascript"></script>
  <script src="../backend/js/scripts/customizer.min.js" type="text/javascript"></script>
  <!-- END STACK JS-->
 
  @yield('scripts')
</body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/vertical-menu-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Dec 2017 10:15:12 GMT -->
</html>