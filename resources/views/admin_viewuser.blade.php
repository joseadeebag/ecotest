@extends('layouts.ba')
@section('title')
   Admin Backend
@endsection

@section('styles')


@endsection

@section('content')

 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">View User</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Users</a>
                </li>
                <li class="breadcrumb-item active">User 
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
      {{--     <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
            <div role="group" class="btn-group">
              <button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings icon-left"></i> Settings</button>
              <div aria-labelledby="btnGroupDrop1" class="dropdown-menu"><a href="card-bootstrap.html" class="dropdown-item">Bootstrap Cards</a>
                <a href="component-buttons-extended.html" class="dropdown-item">Buttons Extended</a>
              </div>
            </div>
            <a href="full-calender-basic.html" class="btn btn-outline-primary"><i class="ft-mail"></i></a>
            <a href="timeline-center.html" class="btn btn-outline-primary"><i class="ft-pie-chart"></i></a>
          </div> --}}
        </div>
      </div>
      <div class="content-body">
        <!-- User Profile Cards -->
        <section id="user-profile-cards" class="row mt-2">
         {{--  <div class="col-12">
            <h4 class="text-uppercase">User Profile Cards</h4>
            <p>User profile cards with border & shadow variant.</p>
          </div> --}}
          <div class="col-xl-2 col-md-6 col-12">
            <div class="card">
              {{-- <div class="text-center">
                <div class="card-body">
                  <img src="../backend/images/portrait/medium/avatar-m-4.png" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Frances Butler</h4>
                  <h6 class="card-subtitle text-muted">Technical Lead</h6>
                </div>
                <div class="card-body">
                  <button type="button" class="btn btn-danger mr-1"><i class="fa fa-plus"></i> Follow</button>
                  <button type="button" class="btn btn-primary mr-1"><i class="ft-user"></i> Profile</button>
                </div>
              </div>
              <div class="list-group list-group-flush">
                <a href="#" class="list-group-item"><i class="fa fa-briefcase"></i> Overview</a>
                <a href="#" class="list-group-item"><i class="ft-mail"></i> Email</a>
                <a href="#" class="list-group-item"><i class="ft-check-square"></i> Task</a>
                <a href="#" class="list-group-item"> <i class="ft-message-square"></i> Calender</a>
              </div> --}}
            </div>
          </div>
          <div class="col-xl-8 {{-- col-md-6 col-12 --}}">
            <div class="card card border-teal border-lighten-2">
              <div class="text-center">
                <div class="card-body">
                  <img src="<?php if(isset($user[0]->photo) ){
                                        echo $user[0]->photo;
                                    }else{
                                        echo '../img/placeholder.png'; 
                                        }?>" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title"><?php 
                      if(!isset($user[0]->firstname) || !isset($user[0]->lastname )){
                         echo 'Not yet filled';

                      }else{
                        echo $user[0]->firstname.' '.$user[0]->lastname;

                      }

                      
                  ?>  

                  </h4>
                 {{--  <h6 class="card-subtitle text-muted">UI/UX Designer</h6> --}}
                </div>
                <div class="card-body">
                  <button type="button" class="btn btn-danger mr-1"><i class="fa fa-plus"></i> Payment History</button>
                  <button type="button" class="btn btn-primary mr-1" id="login_history"><i class="ft-user"></i> Login History</button>
                </div>
              </div>
              <div class="list-group list-group-flush">
                 <a href="#" class="list-group-item"><i class="fa fa-briefcase"></i> Username: {{$user[0]->username}}  </a>
                <a href="#" class="list-group-item"><i class="ft-mail"></i> Email: {{$user[0]->email}} </a>
                <a href="#" class="list-group-item"><i class="ft-check-square"></i> Phone: {{$user[0]->phone}} </a>
                <a href="#" class="list-group-item"> <i class="ft-message-square"></i> Date Registered:  {{$user[0]->date_entered}}</a>
              </div>
            </div>
          </div>
          <div class="col-xl-2 col-md-6 col-12">
            <div class="card box-shadow-2">
           {{--    <div class="text-center">
                <div class="card-body">
                  <img src="../backend/images/portrait/medium/avatar-m-6.png" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Michelle Howard</h4>
                  <h6 class="card-subtitle text-muted">Managing Director</h6>
                </div>
                <div class="card-body">
                  <button type="button" class="btn btn-danger mr-1"><i class="fa fa-plus"></i> Follow</button>
                  <button type="button" class="btn btn-primary mr-1"><i class="ft-user"></i> Profile</button>
                </div>
              </div>
              <div class="list-group list-group-flush">
                <a href="#" class="list-group-item"><i class="fa fa-briefcase"></i> Overview</a>
                <a href="#" class="list-group-item"><i class="ft-mail"></i> Email</a>
                <a href="#" class="list-group-item"><i class="ft-check-square"></i> Task</a>
                <a href="#" class="list-group-item"> <i class="ft-message-square"></i> Calender</a>
              </div> --}}
            </div>
          </div>
        </section>
        <!--/ User Profile Cards -->
        <!-- User Profile Cards with Stats -->
        {{-- <section id="user-profile-cards-with-stats" class="row mt-2">
          <div class="col-12">
            <h4 class="text-uppercase">User Profile Cards with Stats</h4>
            <p>User profile cards with Stats in border & shadow variant.</p>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card profile-card-with-stats">
              <div class="text-center">
                <div class="card-body">
                  <img src="../backend/images/portrait/medium/avatar-m-8.png" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Linda Holland</h4>
                  <ul class="list-inline list-inline-pipe">
                    <li>@lindah</li>
                    <li>domain.com</li>
                  </ul>
                  <h6 class="card-subtitle text-muted">Managing Director</h6>
                </div>
                <div class="btn-group" role="group" aria-label="Profile example">
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info">
                    <span class="ladda-label"><i class="fa fa-bell-o"></i>
                      <span>12+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info">
                    <span class="ladda-label"><i class="fa fa-heart-o"></i>
                      <span>25</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info">
                    <span class="ladda-label"><i class="fa fa-briefcase"></i>
                      <span>5</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info">
                    <span class="ladda-label"><i class="ft-mail"></i>
                      <span>75+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info">
                    <span class="ladda-label"><i class="fa fa-dashcube"></i>
                      <span>125</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                </div>
                <div class="card-body">
                  <p>Lorem ipsum dolor sit amet, autem imperdiet et nam. Nullam labores
                    id quo, sed ei.</p>
                  <button type="button" class="btn btn-outline-danger btn-md mr-1"><i class="fa fa-plus"></i> Follow</button>
                  <button type="button" class="btn btn-outline-primary btn-md mr-1"><i class="ft-user"></i> Profile</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card profile-card-with-stats border-teal border-lighten-2">
              <div class="text-center">
                <div class="card-body">
                  <img src="../backend/images/portrait/medium/avatar-m-7.png" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Philip Garrett</h4>
                  <ul class="list-inline list-inline-pipe">
                    <li>@philipg</li>
                    <li>domain.com</li>
                  </ul>
                  <h6 class="card-subtitle text-muted">Managing Director</h6>
                </div>
                <div class="btn-group" role="group" aria-label="Profile example">
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-round">
                    <span class="ladda-label"><i class="fa fa-bell-o"></i>
                      <span>12+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-round">
                    <span class="ladda-label"><i class="fa fa-heart-o"></i>
                      <span>25</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-round">
                    <span class="ladda-label"><i class="fa fa-briefcase"></i>
                      <span>5</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-round">
                    <span class="ladda-label"><i class="ft-mail"></i>
                      <span>75+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-round">
                    <span class="ladda-label"><i class="fa fa-dashcube"></i>
                      <span>125</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                </div>
                <div class="card-body">
                  <p>Lorem ipsum dolor sit amet, autem imperdiet et nam. Nullam labores
                    id quo, sed ei.</p>
                  <button type="button" class="btn btn-outline-danger btn-md btn-round mr-1"><i class="fa fa-plus"></i> Follow</button>
                  <button type="button" class="btn btn-outline-primary btn-md btn-round mr-1"><i class="ft-user"></i> Profile</button>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-md-6 col-12">
            <div class="card profile-card-with-stats box-shadow-2">
              <div class="text-center">
                <div class="card-body">
                  <img src="../backend/images/portrait/medium/avatar-m-10.png" class="rounded-circle  height-150"
                  alt="Card image">
                </div>
                <div class="card-body">
                  <h4 class="card-title">Christine Wood</h4>
                  <ul class="list-inline list-inline-pipe">
                    <li>@christine</li>
                    <li>domain.com</li>
                  </ul>
                  <h6 class="card-subtitle text-muted">Managing Director</h6>
                </div>
                <div class="btn-group" role="group" aria-label="Profile example">
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-square">
                    <span class="ladda-label"><i class="fa fa-bell-o"></i>
                      <span>12+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-square">
                    <span class="ladda-label"><i class="fa fa-heart-o"></i>
                      <span>25</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-square">
                    <span class="ladda-label"><i class="fa fa-briefcase"></i>
                      <span>5</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-square">
                    <span class="ladda-label"><i class="ft-mail"></i>
                      <span>75+</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                  <button type="button" class="btn btn-float box-shadow-0 btn-outline-info btn-square">
                    <span class="ladda-label"><i class="fa fa-dashcube"></i>
                      <span>125</span>
                    </span>
                    <span class="ladda-spinner"></span>
                  </button>
                </div>
                <div class="card-body">
                  <p>Lorem ipsum dolor sit amet, autem imperdiet et nam. Nullam labores
                    id quo, sed ei.</p>
                  <button type="button" class="btn btn-outline-danger btn-md btn-square mr-1"><i class="fa fa-plus"></i> Follow</button>
                  <button type="button" class="btn btn-outline-primary btn-md btn-square mr-1"><i class="ft-user"></i> Profile</button>
                </div>
              </div>
            </div>
          </div>
        </section> --}}
        <!-- User Profile Cards with Stats -->
     
      </div>
    </div>
  </div>
  



@endsection

@section('scripts')
  
       <script type="text/javascript">
        $("#login_history").on('click', function(e) {
          // e.preventDefault();
         //var value=  $('#movietype :selected').val();
       //  alert(value);
        window.location.href = '/login_history/'+{{$user[0]->id}};

       });
    

      </script>
 
@endsection


