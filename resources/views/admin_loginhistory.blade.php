
@extends('layouts.ba')
@section('title')
   Admin Backend
@endsection

@section('styles')
   
@endsection

@section('content')

   <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title mb-0">Login History</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Login History</a>
                </li>
                <li class="breadcrumb-item active">History 
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right col-md-6 col-12">
        {{--   <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right">
            <div role="group" class="btn-group">
              <button id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false" class="btn btn-outline-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings icon-left"></i> Settings</button>
              <div aria-labelledby="btnGroupDrop1" class="dropdown-menu"><a href="card-bootstrap.html" class="dropdown-item">Bootstrap Cards</a>
                <a href="component-buttons-extended.html" class="dropdown-item">Buttons Extended</a>
              </div>
            </div>
            <a href="full-calender-basic.html" class="btn btn-outline-primary"><i class="ft-mail"></i></a>
            <a href="timeline-center.html" class="btn btn-outline-primary"><i class="ft-pie-chart"></i></a>
          </div> --}}
        </div>
      </div>
      <div class="content-body">
        <!-- Search form-->
        <div id="search-videos" class="card overflow-hidden">
          <div class="card-header">
            <h4 class="card-title">Login History</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
              <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                <li><a data-action="close"><i class="ft-x"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="card-content">
            <!--Search Form-->
            {{-- <div class="card-body pb-0">
              <fieldset class="form-group position-relative mb-0">
                <input type="text" class="form-control form-control-xl input-xl" id="iconLeft1" placeholder="Search for Users (with name, email address, Username) ...">
                <div class="form-control-position">
                  <i class="ft-search font-medium-4"></i>
                </div>
              </fieldset>
            </div> --}}
            <!--/Search Form-->
            <!--Search Navbar-->
         {{--    <div id="search-nav" class="px-2 py-1">
              <ul class="nav nav nav-inline">
                <li class="nav-item">
                  <a class="nav-link" href="search-website.html"><i class="fa fa-link"></i> Website</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="search-images.html"><i class="fa fa-file-image-o"></i> Images</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="search-videos.html"><i class="fa fa-file-video-o"></i> Videos</a>
                </li>
                
                </li>
              </ul>
            </div> --}}
            <!--/ Search Navbar-->
            <div class="table-responsive" style="padding-top: 20px;">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Login Time</th>
                          <th>Country</th>
                          <th>Ip Address</th>
                          <th>Timezone</th>
                          <th>Regionname</th>
                          <th>Countrycode</th>
                          <th>Latitude</th>
                          <th>Longitude</th>
                           <th>City</th>
                           <th>Isp</th>
                        </tr>
                      </thead>
                      <tbody><?php $i =1;?>
                      @foreach ($user as $m)
                        <tr>
                        
                          <td scope="row">{{$i}}</td>
                          <td>{{$m['login_time']}}</td>
                          <td>{{$m['country']}}</td>
                          <td>{{$m['ip']}}</td>  
                          <td>{{$m['timezone']}}</td>
                          <td>{{$m['regionname']}}</td> 
                          <td>{{$m['countrycode']}}</td>
                          <td>{{$m['latitude']}}</td>
                          <td>{{$m['longitude']}}</td> 
                          <td>{{$m['city']}}</td>
                          <td>{{$m['isp']}}</td>
                        
                        </tr>
                        <?php $i++;?>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                  <nav aria-label="Page navigation mb-3">
                  {!! str_replace('/?', '?', $user->render()) !!}
                  </nav>

              {{--  {!! $user->render() !!}   --}}
           </div>
        </div>
      </div>
    </div>
  </div>

@endsection

 @section('scripts')
  
  @endsection